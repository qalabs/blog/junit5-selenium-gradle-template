# JUnit5 Gradle Selenium Template

- Basic JUnit 5 Gradle Selenium Template with AssertJ and Logback for logging
- Requires Java 11 or higher

## Setting up the environment

- https://blog.qalabs.pl/narzedzia/git-cmder
- https://blog.qalabs.pl/narzedzia/selenium-przegladarki
- https://blog.qalabs.pl/java/przygotowanie-srodowiska

## Running the tests

    ./gradlew clean test
