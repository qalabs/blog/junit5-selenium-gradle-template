package pl.qalabs.blog.junit5.selenium;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

class HelloSeleniumChromeTest {

    private WebDriver driver;
    private static final Logger LOG = LoggerFactory.getLogger(HelloSeleniumChromeTest.class);

    @BeforeEach
    void beforeEach() {
        LOG.info("Starting Chrome");

        // Fix "Invalid Status code=403 text=Forbidden"
        var options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);
    }

    @AfterEach
    void afterEach() {
        LOG.info("Quitting Chrome");
        driver.quit();

    }

    @Test
    void helloSelenium() {
        driver.get("https://qalabs.pl");

        assertAll(
                () -> assertThat(driver.getTitle()).isEqualTo("QA Labs - Warsztaty dla Specjalistów IT"),
                () -> assertThat(driver.findElement(By.cssSelector("#page-top > header div.intro-heading")).getText()).isNotBlank(),
                () -> assertThat(driver.findElement(By.cssSelector("#page-top > header div.intro-lead-in")).getText()).isNotBlank()
        );
    }
}
