package pl.qalabs.blog.junit5.selenium;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

class HelloSeleniumEdgeTest {

    private WebDriver driver;
    private static final Logger LOG = LoggerFactory.getLogger(HelloSeleniumEdgeTest.class);


    @BeforeEach
    void beforeEach() {
        LOG.info("Starting Edge");
        driver = new EdgeDriver();
    }

    @AfterEach
    void afterEach() {
        LOG.info("Quitting Edge");
        driver.quit();
    }

    @Test
    void helloSelenium() {
        driver.get("https://qalabs.pl");

        assertAll(
                () -> assertThat(driver.getTitle()).isEqualTo("QA Labs - Warsztaty dla Specjalistów IT"),
                () -> assertThat(driver.findElement(By.cssSelector("#page-top > header div.intro-heading")).getText()).isNotBlank(),
                () -> assertThat(driver.findElement(By.cssSelector("#page-top > header div.intro-lead-in")).getText()).isNotBlank()
        );
    }
}
