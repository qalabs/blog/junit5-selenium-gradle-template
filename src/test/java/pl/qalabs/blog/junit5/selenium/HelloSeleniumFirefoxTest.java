package pl.qalabs.blog.junit5.selenium;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

class HelloSeleniumFirefoxTest {

    private WebDriver driver;
    private static final Logger LOG = LoggerFactory.getLogger(HelloSeleniumFirefoxTest.class);


    @BeforeEach
    void beforeEach() {
        LOG.info("Starting Firefox");
        driver = new FirefoxDriver();
    }

    @AfterEach
    void afterEach() {
        LOG.info("Quitting Firefox");
        driver.quit();
    }

    @Test
    void helloSelenium() {
        driver.get("https://qalabs.pl");

        assertAll(
                () -> assertThat(driver.getTitle()).isEqualTo("QA Labs - Warsztaty dla Specjalistów IT"),
                () -> assertThat(driver.findElement(By.cssSelector("#page-top > header div.intro-heading")).getText()).isNotBlank(),
                () -> assertThat(driver.findElement(By.cssSelector("#page-top > header div.intro-lead-in")).getText()).isNotBlank()
        );
    }
}
